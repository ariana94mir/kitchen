package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Recipe {

    private final String recipeName;
    private ArrayList<Ingredient> recipeIngredient = new ArrayList<>();

    public Recipe(@JsonProperty("recipeName") String recipeName) {
        this.recipeName = recipeName;
    }

    // getter
    public String getRecipeName() {
        return recipeName;
    }

    public ArrayList<Ingredient> getRecipeIngredient() {
        return recipeIngredient;
    }


    // to String
    @Override
    public String toString() {
        return "Recipe{" +
                "recipeName='" + recipeName + '\'' +
                ", recipeIngredient=" + recipeIngredient +
                '}';
    }


}
