package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Ingredient {

    private  final String ingredientName;
    private  int ingredientCount;

    public Ingredient(@JsonProperty("name") String ingredientName,
                      @JsonProperty("count") int ingredientCount) {
        this.ingredientName = ingredientName;
        this.ingredientCount = ingredientCount;
    }

    /*public Ingredient(@JsonProperty("name") String ingredientName) {
        this.ingredientName = ingredientName;
        this.ingredientCount = 1;
    }*/


    // getter

    public String getIngredientName() {
        return ingredientName;
    }

    public int getIngredientCount() {
        return ingredientCount;
    }

    // setter

    public void setIngredientCount(int ingredientCount) {
        this.ingredientCount = ingredientCount;
    }


    // toString
    @Override
    public String toString() {
        return "Ingredient{" +
                "ingredientName='" + ingredientName + '\'' +
                ", ingredientCount=" + ingredientCount +
                '}';
    }



}
