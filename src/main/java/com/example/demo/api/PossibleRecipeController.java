package com.example.demo.api;

import com.example.demo.model.Recipe;
import com.example.demo.service.UIservice.UIPossibleRecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RequestMapping("/api/v1/possible")
@RestController
public class PossibleRecipeController {

    private final UIPossibleRecipeService possibleRecipeService;

    @Autowired
    public PossibleRecipeController(@Qualifier("possibleRecipeService") UIPossibleRecipeService possibleRecipeService) {
        this.possibleRecipeService = possibleRecipeService;
    }


    // get possible recipe list

    @GetMapping
    public ArrayList<Recipe> getPossibleRecipeList(){
        return possibleRecipeService.findPossibleRecipeList();
    }



}
