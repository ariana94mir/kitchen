package com.example.demo.api;

import com.example.demo.model.Ingredient;
import com.example.demo.service.UIservice.UIMyListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RequestMapping("/api/v1/ingredient")
@RestController
public class MyListController {

    private final UIMyListService myListService;


    @Autowired
    public MyListController(@Qualifier("myListService") UIMyListService myListService) {
        this.myListService = myListService;
    }

    //post
    @PostMapping
    public void addIngredientToMyList(@RequestBody Ingredient ingredient){
        myListService.insertIngredientToMyList(ingredient);
    }

    // get
    @GetMapping
    public ArrayList<Ingredient> getMyIngredientList(){

        return myListService.getMyList();
    }

    @GetMapping (path = "{ingredientName}")
    public Ingredient getIngredientByName(@PathVariable("ingredientName") String ingredientName){
        return myListService.getMyListByName(ingredientName).
                orElse(null);
    }

    //delete
    @DeleteMapping(path = "{ingredientName}")
    public void deleteIngredientByName(@PathVariable("ingredientName") String ingredientName){
        myListService.deleteIngredientByName(ingredientName);
    }


    //update

    @PutMapping(path = "{ingredientName}")
    public void updateIngredientByName(@PathVariable("ingredientName") String ingredientName,
                                       @RequestBody Ingredient newIngredient){
        myListService.updateIngredientByName(ingredientName, newIngredient);
    }


}
