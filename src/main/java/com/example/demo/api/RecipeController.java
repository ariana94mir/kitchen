package com.example.demo.api;


import com.example.demo.model.Ingredient;
import com.example.demo.model.Recipe;
import com.example.demo.service.UIservice.UIRecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RequestMapping("/api/v1/recipe")
@RestController
public class RecipeController {

    private final UIRecipeService recipeService;

    @Autowired
    public RecipeController(@Qualifier("recipeService") UIRecipeService recipeService) {
        this.recipeService = recipeService;
    }

    // post new recipe to recipe list
    @PostMapping
    public void addNewRecipe(@RequestBody Recipe recipe){
        recipeService.addNewRecipe(recipe);
    }

    // get the recipe list
    @GetMapping
    public ArrayList<Recipe> getRecipeList(){
        return recipeService.getRecipeList();
    }

    // get the recipe by name

    @GetMapping(path = "{recipeName}")
    public Recipe getRecipeByName(@PathVariable("recipeName") String recipeName){
        return recipeService.getRecipeByName(recipeName).
                orElse(null);
    }

    // delete the recipe by name

    @DeleteMapping(path = "{recipeName}")
    public void deleteRecipeByName(@PathVariable("recipeName") String recipeName){
        recipeService.deleteRecipeByName(recipeName);
    }

    // post new ingredient in to the recipe by recipe's name

    @PostMapping(path = "{recipeName}")
    public void addIngredientToRecipeByRecipeName(@PathVariable("recipeName") String recipeName,
                                                  @RequestBody Ingredient ingredient){
        recipeService.addIngredientToRecipeByName(recipeName, ingredient);
    }

    // get the ingredient by name from recipe

    @GetMapping(path = "{recipeName}/{ingredientName}")
    public Ingredient getIngredientByNameFromRecipe(@PathVariable("recipeName") String recipeName,
                                                    @PathVariable("ingredientName") String ingredientName){
        return recipeService.getIngredientByNameFromRecipe(recipeName,ingredientName).orElse(null);
    }



}
