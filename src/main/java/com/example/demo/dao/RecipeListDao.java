package com.example.demo.dao;

import com.example.demo.dao.UIdao.UIRecipeListDao;
import com.example.demo.model.Ingredient;
import com.example.demo.model.Recipe;
import com.fasterxml.jackson.databind.introspect.TypeResolutionContext;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository("RecipeInMemory")
public class RecipeListDao implements UIRecipeListDao {

    private ArrayList<Recipe> recipeList = new ArrayList<Recipe>();


    // add new recipe
    @Override
    public boolean addNewRecipeToList(Recipe recipe) {

        recipeList.add(recipe);
        return true;
    }

    // get the recipe list
    @Override
    public ArrayList<Recipe> getRecipeList() {
        return recipeList;
    }


    // get the recipe by name
    @Override
    public Optional<Recipe> getRecipeByName(String recipeName) {
       return recipeList.
               stream().
               filter(recipe -> recipe.getRecipeName().equals(recipeName)).
               findFirst();
    }

    @Override
    public boolean deleteRecipeByName(String recipeName) {
        var finedRecipe = getRecipeByName(recipeName);
        if(finedRecipe.isEmpty()){
            return false;
        }
        else{
            recipeList.remove(finedRecipe.get());
            return true;
        }
    }

    @Override
    public boolean addIngredientToRecipeByName(String recipeName, Ingredient ingredient) {

        var finedRecipe = getRecipeByName(recipeName);
        if(finedRecipe.isEmpty()){
            return false;
        }
        else{
            finedRecipe.get().getRecipeIngredient().add(ingredient);
            return true;
        }
    }

    @Override
    public ArrayList<Ingredient> getIngredientOfRecipeByName(String recipeName){
        var finedRecipe = getRecipeByName(recipeName);
        if(finedRecipe.isEmpty()){
            return null;
        }
        else{
            return finedRecipe.get().getRecipeIngredient();
        }
    }

    @Override
    public Optional<Ingredient> getIngredientByNameFromRecipe(String recipeName, String ingredientName){
        var finedRecipe = getRecipeByName(recipeName);
        if(finedRecipe.isEmpty()){

            return Optional.empty();
        }
        else{
            return finedRecipe.
                    get().
                    getRecipeIngredient().
                    stream().
                    filter(ing -> ing.getIngredientName().equals(ingredientName)).
                    findFirst();
        }
    }

}
