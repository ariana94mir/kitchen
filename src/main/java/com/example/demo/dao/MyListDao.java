package com.example.demo.dao;

import com.example.demo.dao.UIdao.UIMyListDao;
import com.example.demo.model.Ingredient;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

@Repository("inMemory")
public class MyListDao implements UIMyListDao {

    private ArrayList<Ingredient> myList = new ArrayList<Ingredient>();


    @Override
    public Boolean addIngredientToList(Ingredient ingredient) {

        myList.add(new Ingredient(ingredient.getIngredientName(), ingredient.getIngredientCount()));
        return true;
    }

    // getter

    @Override
    public ArrayList<Ingredient> getMyList() {
        return myList;
    }

    @Override
    public Optional<Ingredient> getIngredientByName(String ingredientName) {
        return myList.stream()
                .filter(ingredient ->  ingredient.getIngredientName().equals(ingredientName))
                .findFirst();
    }


    //delete by name
    @Override
    public Boolean deleteIngredientByName(String ingredientName) {
        var finedIngredient = getIngredientByName(ingredientName);
        if(finedIngredient.isEmpty()){
            return false;
        }
        else{
            myList.remove(finedIngredient.get());
            return true;
        }
    }

    @Override
    public Boolean updateIngredientByName(String ingredientName, Ingredient ingredient) {
        var finedIngredient = getIngredientByName(ingredientName);

        if(finedIngredient.isEmpty()){
            return false;
        }
        else{
            int indexOfFinedIngredient = myList.indexOf(finedIngredient.get());
            myList.set(indexOfFinedIngredient, ingredient);
            return true;
        }
    }


}
