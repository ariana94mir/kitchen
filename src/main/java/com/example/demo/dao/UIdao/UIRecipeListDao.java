package com.example.demo.dao.UIdao;

import com.example.demo.model.Ingredient;
import com.example.demo.model.Recipe;

import java.util.ArrayList;
import java.util.Optional;

public interface UIRecipeListDao {

boolean addNewRecipeToList(Recipe recipe);

    ArrayList<Recipe> getRecipeList();

    Optional<Recipe> getRecipeByName(String recipeName);

    boolean deleteRecipeByName(String recipeName);

    boolean addIngredientToRecipeByName(String recipeName, Ingredient ingredient);

    ArrayList<Ingredient> getIngredientOfRecipeByName(String recipeName);

    Optional<Ingredient> getIngredientByNameFromRecipe(String recipeName, String ingredientName);
}
