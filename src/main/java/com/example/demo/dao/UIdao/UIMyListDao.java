package com.example.demo.dao.UIdao;

import com.example.demo.model.Ingredient;

import java.util.ArrayList;
import java.util.Optional;

public interface UIMyListDao {

    Boolean addIngredientToList(Ingredient ingredient);

    ArrayList<Ingredient> getMyList();

    Optional<Ingredient> getIngredientByName(String ingredientName);

    Boolean deleteIngredientByName(String ingredientName);

    Boolean updateIngredientByName(String ingredientName, Ingredient ingredient);
}
