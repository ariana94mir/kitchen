package com.example.demo.service;

import com.example.demo.dao.UIdao.UIMyListDao;
import com.example.demo.dao.UIdao.UIRecipeListDao;
import com.example.demo.model.Ingredient;
import com.example.demo.model.Recipe;
import com.example.demo.service.UIservice.UIMyListService;
import com.example.demo.service.UIservice.UIPossibleRecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.stream.Collectors;

@Service("possibleRecipeService")
public class PossibleRecipeService implements UIPossibleRecipeService {

    private final UIMyListDao myList;
    private final UIRecipeListDao recipeList;


    @Autowired
    public PossibleRecipeService(@Qualifier("inMemory") UIMyListDao myList,
                                 @Qualifier("RecipeInMemory") UIRecipeListDao recipeList) {
        this.myList = myList;
        this.recipeList = recipeList;
    }

    // finding possible Recipe List

    @Override
    public ArrayList<Recipe> findPossibleRecipeList(){
        var recipes = recipeList.getRecipeList();
        var myIngredients = myList.getMyList();

        var possibleList = recipes.stream().filter(rec -> {
            return possibleRecipe(myIngredients, rec);
        }).collect(Collectors.toList());

        return (ArrayList<Recipe>) possibleList;
    }

    // private methods
    private boolean possibleRecipe(ArrayList<Ingredient> myIngredients, Recipe rec) {
        if(rec.getRecipeIngredient().isEmpty()){
            return false;
        }
        else{
            boolean thisRecipePossible = true;
            for(Ingredient ing: rec.getRecipeIngredient()){
                var include = isThereSameIngredient(myIngredients, ing);
                if( !(include) ){
                    thisRecipePossible = false;
                    break;
                }
            }
            return thisRecipePossible;
        }

    }

    @Override
    public boolean isThereSameIngredient(ArrayList<Ingredient> myList, Ingredient ingredient){

        var test =myList.stream().
                filter(ing -> {
            boolean thereIs = false;
            if( (ing.getIngredientName().equals(ingredient.getIngredientName() ) )&&
                    (ing.getIngredientCount() > ingredient.getIngredientCount()) ){
                thereIs = true;
            }
            return thereIs;
        }).collect(Collectors.toList());

        if(test.isEmpty()){
            return false;
        }
        else{
            return true;
        }
    }


}
