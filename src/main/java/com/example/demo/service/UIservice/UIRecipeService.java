package com.example.demo.service.UIservice;

import com.example.demo.model.Ingredient;
import com.example.demo.model.Recipe;

import java.util.ArrayList;
import java.util.Optional;

public interface UIRecipeService {

    boolean addNewRecipe(Recipe recipe);

    ArrayList<Recipe> getRecipeList();

    Optional<Recipe> getRecipeByName(String omelet);

    boolean deleteRecipeByName(String recipeName);

    boolean addIngredientToRecipeByName(String recipeName, Ingredient ingredient);

    // get recipe ingredient List by its recipe's name
    ArrayList<Ingredient> getRecipeIngredientListByRecipeName(String recipeName);

    Optional<Ingredient> getIngredientByNameFromRecipe(String recipeName, String ingredientName);
}
