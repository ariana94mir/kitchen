package com.example.demo.service.UIservice;


import com.example.demo.model.Ingredient;
import com.example.demo.model.Recipe;
import org.springframework.stereotype.Service;

import java.util.ArrayList;


public interface UIPossibleRecipeService {
    ArrayList<Recipe> findPossibleRecipeList();

    boolean isThereSameIngredient(ArrayList<Ingredient> myList,
                                  Ingredient ingredient);
}
