package com.example.demo.service.UIservice;

import com.example.demo.model.Ingredient;

import java.util.ArrayList;
import java.util.Optional;

public interface UIMyListService {

    Boolean insertIngredientToMyList (Ingredient ingredient);

    ArrayList<Ingredient> getMyList();

    Optional<Ingredient> getMyListByName(String ingredientName);

    boolean deleteIngredientByName(String ingredientName);

    boolean updateIngredientByName(String ingredientName, Ingredient newIngredient);
}
