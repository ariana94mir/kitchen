package com.example.demo.service;

import com.example.demo.dao.UIdao.UIRecipeListDao;
import com.example.demo.model.Ingredient;
import com.example.demo.model.Recipe;
import com.example.demo.service.UIservice.UIRecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service("recipeService")
public class RecipeService implements UIRecipeService {

   private final UIRecipeListDao recipeListDao;

   @Autowired
   public RecipeService(@Qualifier("RecipeInMemory") UIRecipeListDao recipeListDao) {
        this.recipeListDao = recipeListDao;
   }

// add new recipe
    @Override
    public boolean addNewRecipe(Recipe recipe) {
        return recipeListDao.addNewRecipeToList(recipe);
    }

    // get recipe list
    @Override
    public ArrayList<Recipe> getRecipeList() {
        return recipeListDao.getRecipeList();
    }

    // get the recipe by its name
    @Override
    public Optional<Recipe> getRecipeByName(String recipeName) {
        return recipeListDao.getRecipeByName(recipeName);
    }

    //delete the recipe by its name
    @Override
    public boolean deleteRecipeByName(String recipeName) {
        return recipeListDao.deleteRecipeByName(recipeName);
    }


    // add ingredient to recipe

    @Override
    public boolean addIngredientToRecipeByName(String recipeName, Ingredient ingredient){

        return recipeListDao.addIngredientToRecipeByName(recipeName, ingredient);
    }

    // get recipe ingredient List by its recipe's name
    @Override
    public ArrayList<Ingredient> getRecipeIngredientListByRecipeName(String recipeName) {
        return recipeListDao.getIngredientOfRecipeByName(recipeName);
    }

    // get Ingredient by name from recipe
    @Override
    public Optional<Ingredient> getIngredientByNameFromRecipe(String recipeName, String ingredientName) {
        return recipeListDao.getIngredientByNameFromRecipe(recipeName, ingredientName);
    }
}
