package com.example.demo.service;

import com.example.demo.dao.UIdao.UIMyListDao;
import com.example.demo.model.Ingredient;
import com.example.demo.service.UIservice.UIMyListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service("myListService")
public class MyListService implements UIMyListService {

    private UIMyListDao myList;

    @Autowired
    public MyListService(@Qualifier("inMemory") UIMyListDao myList) {
        this.myList = myList;
    }


    // Add Ingredient
    @Override
    public Boolean insertIngredientToMyList(Ingredient ingredient){

        return myList.addIngredientToList(ingredient);
    }

    // getter
    @Override
    public ArrayList<Ingredient> getMyList(){
        return myList.getMyList();
    }

    @Override
    public Optional<Ingredient> getMyListByName(String ingredientName) {
        return myList.getIngredientByName(ingredientName);
    }

    @Override
    public boolean deleteIngredientByName(String ingredientName) {

        return myList.deleteIngredientByName(ingredientName);
    }

    @Override
    public boolean updateIngredientByName(String ingredientName, Ingredient newIngredient) {
        return myList.updateIngredientByName(ingredientName, newIngredient);
    }

}
