package com.example.demo.serviceTest;

import com.example.demo.dao.RecipeListDao;
import com.example.demo.dao.UIdao.UIRecipeListDao;
import com.example.demo.model.Ingredient;
import com.example.demo.model.Recipe;
import com.example.demo.service.RecipeService;
import com.example.demo.service.UIservice.UIRecipeService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class RecipeIngredientServiceTest {

    private final UIRecipeListDao recipeList = new RecipeListDao();
    private final UIRecipeService recipeIngredientService;


    public RecipeIngredientServiceTest() {
        this.recipeIngredientService = new RecipeService(recipeList);
    }


    // add ingredient to the recipe by recipe name test

    @Test
    public void addIngredientToTheEmptyRecipeListTest1(){
        //
        var ingredient1 = new Ingredient("egg",2);
        //
        boolean isItAdded = recipeIngredientService.addIngredientToRecipeByName("omelet", ingredient1);
        //
        Assert.assertFalse(isItAdded);
    }

    @Test
    public void addIngredientToRecipeListTest1(){
        //
        var ingredient1 = new Ingredient("egg",2);
        var recipe1 = new Recipe("omelet");
        //
        recipeList.addNewRecipeToList(recipe1);

        boolean isItAdded = recipeIngredientService.addIngredientToRecipeByName("omelet", ingredient1);
        //
        Assert.assertTrue(isItAdded);
    }

    @Test
    public void addIngredientToRecipeListTest2(){
        //
        var ingredient1 = new Ingredient("egg",2);
        var ingredient2 = new Ingredient("tomato",2);
        var recipe1 = new Recipe("omelet");
        //
        recipeList.addNewRecipeToList(recipe1);

        recipeList.getRecipeList().stream().forEach(System.out::println);

        recipeIngredientService.addIngredientToRecipeByName("omelet", ingredient1);
        recipeIngredientService.addIngredientToRecipeByName("omelet", ingredient2);

        System.out.println("----------------after adding--------------");
        recipeList.getRecipeList().stream().forEach(System.out::println);

        var recipeIngredientList = recipeList.
                getRecipeByName("omelet").
                get().getRecipeIngredient();
        //
        Assert.assertEquals(2, recipeIngredientList.size());
    }

    // get recipe ingredient list by its recipe name
    @Test
    public void getRecipeIngredientListFromEmptyRecipeListTest(){
        //


        //
        var isThereAnyRecipe = recipeIngredientService.getRecipeIngredientListByRecipeName("recipeName");
        //
        Assert.assertNull(isThereAnyRecipe);
    }

    @Test
    public void getRecipeIngredientListByRecipeTest1() {
        //
        Recipe recipe1 = new Recipe("omelet");
        //
        recipeList.addNewRecipeToList(recipe1);

        var isThereAnyIngredient = recipeIngredientService.
                getRecipeIngredientListByRecipeName("omelet");
        //
        Assert.assertTrue(isThereAnyIngredient.isEmpty());
    }

    // get ingredient by name from recipe test

    @Test
    public void getIngredientByNameFromEmptyRecipeListTest(){
        //

        //
        var isThereAnyIngredient = recipeIngredientService.getIngredientByNameFromRecipe("recipeName", "ingredientName");
        //
        Assert.assertNotNull(isThereAnyIngredient);
    }

    @Test
    public void getIngredientByNameFromEmptyRecipeListTest2(){
        //

        //
        var isThereAnyIngredient = recipeIngredientService.getIngredientByNameFromRecipe("recipeName", "ingredientName");
        //
        Assert.assertTrue(isThereAnyIngredient.isEmpty());
    }

    @Test
    public void getIngredientByNameFromEmptyRecipeTest1(){
        //
        Recipe recipe1 = new Recipe("omelet");

        //
        recipeIngredientService.addNewRecipe(recipe1);
        var isThereAnyIngredient = recipeIngredientService.getIngredientByNameFromRecipe("recipeName", "ingredientName");
        //
        Assert.assertTrue(isThereAnyIngredient.isEmpty());
    }

    @Test
    public void getIngredientByNameFromRecipeTest1(){
        //
        Recipe recipe1 = new Recipe("omelet");
        Ingredient ingredient1 = new Ingredient("egg",2);

        //
        recipeIngredientService.addNewRecipe(recipe1);
        recipeIngredientService.addIngredientToRecipeByName(recipe1.getRecipeName(), ingredient1);
        var isThereAnyIngredient = recipeIngredientService.
                getIngredientByNameFromRecipe(recipe1.getRecipeName(), ingredient1.getIngredientName());
        //
        Assert.assertFalse(isThereAnyIngredient.isEmpty());
    }

    @Test
    public void getIngredientByNameFromRecipeTest2(){
        //
        Recipe recipe1 = new Recipe("omelet");
        Ingredient ingredient1 = new Ingredient("egg",2);

        //
        recipeIngredientService.addNewRecipe(recipe1);
        recipeIngredientService.addIngredientToRecipeByName(recipe1.getRecipeName(), ingredient1);
        var isThereAnyIngredient = recipeIngredientService.
                getIngredientByNameFromRecipe(recipe1.getRecipeName(), ingredient1.getIngredientName());
        //
        Assert.assertTrue(isThereAnyIngredient.get().getIngredientName().equals(ingredient1.getIngredientName()));
    }

}
