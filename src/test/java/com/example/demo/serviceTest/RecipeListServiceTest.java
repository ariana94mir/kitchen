package com.example.demo.serviceTest;

import com.example.demo.dao.RecipeListDao;
import com.example.demo.dao.UIdao.UIRecipeListDao;
import com.example.demo.model.Recipe;
import com.example.demo.service.RecipeService;
import com.example.demo.service.UIservice.UIRecipeService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class RecipeListServiceTest {

    private final UIRecipeListDao recipeList = new RecipeListDao();
    private final UIRecipeService recipeService;


    public RecipeListServiceTest() {
        this.recipeService = new RecipeService(recipeList);
    }


    // add recipe to recipe List
    @Test
    public void addRecipeToRecipeListTest1(){
        //
        var recipe1 = new Recipe("omelet");

        //
        boolean isItAdded = recipeService.addNewRecipe(recipe1);
        //
        Assert.assertTrue(isItAdded);
    }

    @Test
    public void addRecipeToRecipeListTest2(){
        //
        var recipe1 = new Recipe("omelet");

        //
        recipeService.addNewRecipe(recipe1);
        //
        Assert.assertEquals(1, recipeList.getRecipeList().size());
    }

    @Test
    public void addRecipeToRecipeListTest3(){
        var recipe1 = new Recipe("omelet");
        var recipe2 = new Recipe("jomlet");

        //
        recipeService.addNewRecipe(recipe1);
        recipeService.addNewRecipe(recipe2);
        //
        Assert.assertEquals(2, recipeList.getRecipeList().size());
    }

    // get the recipe list

    @Test
    public void getEmptyRecipeListTest(){
        //

        //
        var isItNull = recipeService.getRecipeList();
        //
        Assert.assertNotNull(isItNull);
    }

    @Test
    public void getRecipeListTest1(){
        //
        var recipe1 = new Recipe("omelet");

        //
        recipeService.addNewRecipe(recipe1);
        var recipeList = recipeService.getRecipeList();
        //
        Assert.assertEquals(1, recipeList.size());
    }

    @Test
    public void getRecipeListTest2(){
        //
        var recipe1 = new Recipe("omelet");
        var recipe2 = new Recipe("jomlet");

        //
        recipeService.addNewRecipe(recipe1);
        recipeService.addNewRecipe(recipe2);
        var recipeList = recipeService.getRecipeList();
        //
        Assert.assertEquals(2, recipeList.size());
    }

    // get recipe by name

    @Test
    public void getRecipeByNameFromEmptyRecipeList1(){
        //

        //
        var finedRecipe =recipeService.getRecipeByName("omelet");
        //
        Assert.assertNotNull(finedRecipe);
    }


    @Test
    public void getRecipeByNameFromEmptyRecipeList2(){
        //

        //
        var finedRecipe =recipeService.getRecipeByName("omelet");
        //
        Assert.assertTrue(finedRecipe.isEmpty());
    }

    @Test
    public void getRecipeByNameTest1(){
        //
        var recipe1 = new Recipe("omelet");
        var recipe2 = new Recipe("jomlet");

        //
        recipeService.addNewRecipe(recipe1);
        recipeService.addNewRecipe(recipe2);

        var finedRecipe = recipeService.getRecipeByName("omelet");
        //
        Assert.assertFalse(finedRecipe.isEmpty());
    }

    @Test
    public void getRecipeByNameTest2() {
        //
        var recipe1 = new Recipe("omelet");
        var recipe2 = new Recipe("jomlet");

        //
        recipeService.addNewRecipe(recipe1);
        recipeService.addNewRecipe(recipe2);

        var finedRecipe = recipeService.getRecipeByName("omelet");
        //

        Assert.assertTrue(finedRecipe.get().getRecipeName().equals("omelet"));
    }

    // delete recipe by name

    @Test
    public void deleteFromEmptyRecipeListTest(){
        //

        //
        boolean isItDeleted = recipeService.deleteRecipeByName("omelet");
        //
        Assert.assertFalse(isItDeleted);
    }

    @Test
    public void deleteRecipeByNameTest1(){
        //
        var recipe1 = new Recipe("omelet");
        //
        recipeService.addNewRecipe(recipe1);

        recipeService.getRecipeList().stream().forEach(recipe -> System.out.println(recipe.toString()));

        boolean isItDeleted = recipeService.deleteRecipeByName("omelet");

        System.out.println("-------------------after delete---------------");
        recipeService.getRecipeList().stream().forEach(recipe -> System.out.println(recipe.toString()));
        //
        Assert.assertTrue(isItDeleted);
    }

    @Test
    public void deleteRecipeByNameTest2(){
        //
        var recipe1 = new Recipe("omelet");
        var recipe2 = new Recipe("jomlet");

        //
        recipeService.addNewRecipe(recipe1);
        recipeService.addNewRecipe(recipe2);

        recipeService.getRecipeList().stream().forEach(recipe -> System.out.println(recipe.toString()));

        boolean isItDeleted = recipeService.deleteRecipeByName("omelet");

        System.out.println("-------------------after delete---------------");
        recipeService.getRecipeList().stream().forEach(recipe -> System.out.println(recipe.toString()));
        //
        Assert.assertEquals(1, recipeService.getRecipeList().size());
    }
}
