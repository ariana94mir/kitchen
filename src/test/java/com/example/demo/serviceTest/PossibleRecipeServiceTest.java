package com.example.demo.serviceTest;

import com.example.demo.dao.MyListDao;
import com.example.demo.dao.RecipeListDao;
import com.example.demo.dao.UIdao.UIMyListDao;
import com.example.demo.dao.UIdao.UIRecipeListDao;
import com.example.demo.model.Ingredient;
import com.example.demo.model.Recipe;
import com.example.demo.service.PossibleRecipeService;
import com.example.demo.service.UIservice.UIPossibleRecipeService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class PossibleRecipeServiceTest {
    private final UIMyListDao myListDao = new MyListDao();
    private final UIRecipeListDao recipeListDao = new RecipeListDao();
    private final UIPossibleRecipeService possibleService;


    public PossibleRecipeServiceTest() {
        this.possibleService = new PossibleRecipeService(myListDao, recipeListDao);
    }

    @Test
    public void isThereSameIngredientTest1(){
        //
        Ingredient ingredient1 = new Ingredient("apple",2);
        Ingredient ingredient2 = new Ingredient("egg",2);
        Ingredient testIngredient = new Ingredient("apple",3);

        //
        myListDao.addIngredientToList(ingredient1);
        myListDao.addIngredientToList(ingredient2);

        var isThere = possibleService.isThereSameIngredient(myListDao.getMyList(),testIngredient);
        //
        Assert.assertTrue(isThere);
    }

    @Test
    public void isThereSameIngredientTest2(){
        //
        Ingredient ingredient1 = new Ingredient("apple",2);
        Ingredient ingredient2 = new Ingredient("egg",2);
        Ingredient testIngredient = new Ingredient("apple",1);

        //
        myListDao.addIngredientToList(ingredient1);
        myListDao.addIngredientToList(ingredient2);

        var isThere = possibleService.isThereSameIngredient(myListDao.getMyList(),testIngredient);
        //
        Assert.assertFalse(isThere);
    }

    @Test
    public void isThereSameIngredientTest3(){
        //
        Ingredient ingredient1 = new Ingredient("apple",2);
        Ingredient ingredient2 = new Ingredient("egg",2);
        Ingredient testIngredient = new Ingredient("pear",3);

        //
        myListDao.addIngredientToList(ingredient1);
        myListDao.addIngredientToList(ingredient2);

        var isThere = possibleService.isThereSameIngredient(myListDao.getMyList(),testIngredient);
        //
        Assert.assertFalse(isThere);
    }

    @Test
    public void isThereSameIngredientTest4(){
        //
        Ingredient ingredient1 = new Ingredient("apple",2);
        Ingredient ingredient2 = new Ingredient("egg",2);
        Ingredient testIngredient = new Ingredient("pear",3);

        //

        var isThere = possibleService.isThereSameIngredient(myListDao.getMyList(),testIngredient);
        //
        Assert.assertFalse(isThere);
    }

    // finding possible recipe list

    @Test
    public void findingPossibleRecipeListTest1(){
        //

        //
        var possibleList = possibleService.findPossibleRecipeList();
        //
        Assert.assertTrue(possibleList.isEmpty());
    }


    @Test
    public void findingPossibleRecipeListTest2(){
        //
        Ingredient ingredient1 = new Ingredient("apple",2);
        Ingredient ingredient2 = new Ingredient("egg",2);
        Ingredient ingredient3 = new Ingredient("pear",3);

        //
        myListDao.addIngredientToList(ingredient1);
        myListDao.addIngredientToList(ingredient2);

        var possibleList = possibleService.findPossibleRecipeList();
        //
        Assert.assertTrue(possibleList.isEmpty());
    }

    @Test
    public void findingPossibleRecipeListTest3(){
        //
        Ingredient ingredient1 = new Ingredient("apple",2);
        Ingredient ingredient2 = new Ingredient("egg",2);
        Ingredient ingredient3 = new Ingredient("pear",3);
        Recipe recipe1 = new Recipe("omelet");

        //
        myListDao.addIngredientToList(ingredient1);
        myListDao.addIngredientToList(ingredient2);
        recipeListDao.addNewRecipeToList(recipe1);

        var possibleList = possibleService.findPossibleRecipeList();
        possibleList.stream().forEach(System.out::println);
        //
        Assert.assertTrue(possibleList.isEmpty());
    }

    @Test
    public void findingPossibleRecipeListTest4(){
        //
        Ingredient ingredient1 = new Ingredient("apple",2);
        Ingredient ingredient2 = new Ingredient("egg",2);
        Ingredient ingredient3 = new Ingredient("pear",3);
        Recipe recipe1 = new Recipe("omelet");
        Ingredient recipeIngredient1 = new Ingredient("egg",3);

        //
        myListDao.addIngredientToList(ingredient1);
        myListDao.addIngredientToList(ingredient2);
        recipeListDao.addNewRecipeToList(recipe1);
        recipeListDao.addIngredientToRecipeByName(recipe1.getRecipeName(), recipeIngredient1);

        var possibleList = possibleService.findPossibleRecipeList();
        possibleList.stream().forEach(System.out::println);
        //
        Assert.assertTrue(possibleList.isEmpty());
    }


}
