package com.example.demo.serviceTest;

import com.example.demo.dao.MyListDao;
import com.example.demo.dao.UIdao.UIMyListDao;
import com.example.demo.model.Ingredient;
import com.example.demo.service.MyListService;
import com.example.demo.service.UIservice.UIMyListService;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;

public class MyListServiceTest {

    private final UIMyListService myListService;


    private final UIMyListDao myList = new MyListDao();


    public MyListServiceTest() {

        this.myListService = new MyListService(myList);
    }

// testing insert methode
    @Test
    public void insertToListTest(){
        //
        var ingredient1 = new Ingredient("apple", 2);

        //
        var isItAdd = myListService.insertIngredientToMyList( ingredient1);

        //
        Assertions.assertTrue(isItAdd);
    }

    @Test
    public void insertToMyListTest(){
        //
        var ingredient1 = new Ingredient("apple", 2);

        //
        myListService.insertIngredientToMyList(ingredient1);

        //
        Assert.assertEquals(1,myList.getMyList().size());
    }

    @Test
    public void insertTwoToMyListTest() {
        //
        var ingredient1 = new Ingredient("apple", 2);
        var ingredient2 = new Ingredient("pear", 2);

        //
        myListService.insertIngredientToMyList(ingredient1);
        myListService.insertIngredientToMyList(ingredient2);

        //
        Assert.assertEquals(2, myList.getMyList().size());
    }

    // testing get methode

    @Test
    public void getEmptyMyList(){
        //

        //
        var theList = myListService.getMyList();

        //
        Assertions.assertEquals(0,theList.size());
    }

    @Test
    public void getMyListOneIngredient(){
        //
        var ingredient1 = new Ingredient("apple", 1);

        //
        myListService.insertIngredientToMyList(ingredient1);

        var theList = myListService.getMyList();

        //
        Assertions.assertEquals(1,theList.size());
    }

    @Test
    public void getMyListTwoIngredient(){
        //
        var ingredient1 = new Ingredient("apple", 1);
        var ingredient2 = new Ingredient("egg", 2);

        //
        myListService.insertIngredientToMyList(ingredient1);
        myListService.insertIngredientToMyList(ingredient2);

        var theList = myListService.getMyList();

        //
        Assertions.assertEquals(2,theList.size());
    }

    // get by name from list

    @Test
    public void getIngredientByNameFromEmptyMyListTest(){
        //

        //
        var ingredient = myListService.getMyListByName("apple");

        //
        Assertions.assertEquals(0, ingredient.stream().collect(Collectors.toList()).size());
    }

    @Test
    public void getIngredientByNameFromEmptyMyListTest2(){
        //

        //
        var ingredient = myListService.getMyListByName("apple");

        //
        Assert.assertNotNull(ingredient);
    }

    @Test
    public  void getIngredientByNameTest(){
        //
        var ingredient1 = new Ingredient("apple", 1);
        var ingredient2 = new Ingredient("egg", 2);

        //
        myListService.insertIngredientToMyList(ingredient1);
        myListService.insertIngredientToMyList(ingredient2);
        var ingredient = myListService.getMyListByName("apple");

        //
        Assertions.assertEquals(1, ingredient.get().getIngredientCount());
    }

    @Test
    public  void getIngredientByNameTest2(){
        //
        var ingredient1 = new Ingredient("apple", 1);
        var ingredient2 = new Ingredient("egg", 2);

        //
        myListService.insertIngredientToMyList(ingredient1);
        myListService.insertIngredientToMyList(ingredient2);
        var ingredient = myListService.getMyListByName("apple");

        //
        Assertions.assertTrue(ingredient.get().getIngredientName().equals("apple"));
    }

    //Delete from list by name

    @Test
    public void deleteIngredientFromEmptyMyListByNameTest(){
        //

        //
        boolean isItDelete = myListService.deleteIngredientByName("apple");
        //
        Assert.assertFalse(isItDelete);
    }

    @Test
    public void deleteIngredientFromMyListByNameTest1(){
        //
        var ingredient1 = new Ingredient("apple", 1);
        var ingredient2 = new Ingredient("egg", 2);

        //
        myListService.insertIngredientToMyList(ingredient1);
        myListService.insertIngredientToMyList(ingredient2);
        boolean isItDelete = myListService.deleteIngredientByName("apple");

        //
        Assert.assertTrue(isItDelete);
    }

    @Test
    public void deleteIngredientFromMyListByNameTest2(){
        //
        var ingredient1 = new Ingredient("apple", 1);
        var ingredient2 = new Ingredient("egg", 2);

        //
        myListService.insertIngredientToMyList(ingredient1);
        myListService.insertIngredientToMyList(ingredient2);

        myList.getMyList().stream().forEach(ing -> System.out.println(ing.toString()));

        boolean isItDelete = myListService.deleteIngredientByName("apple");

        myList.getMyList().stream().forEach(ing -> System.out.println(ing.toString()));
        //
        Assert.assertEquals(1,myList.getMyList().size());
    }

    // update ingredient in myList by ingredient name

    @Test
    public void updateIngredientInEmptyMyListByNameTest(){
        //
        Ingredient newIngredient = new Ingredient("apple",1);
        //
        var isItUpdate = myListService.
                updateIngredientByName(newIngredient.getIngredientName(), newIngredient);
        //
        Assert.assertFalse(isItUpdate);
    }

    @Test
    public void updateIngredientInMyListByNameTest1(){
        //
        var ingredient1 = new Ingredient("apple", 1);
        var ingredient2 = new Ingredient("egg", 2);
        Ingredient newIngredient = new Ingredient("apple",2);
        //
        myListService.insertIngredientToMyList(ingredient1);
        myListService.insertIngredientToMyList(ingredient2);

        var isItUpdate = myListService.
                updateIngredientByName(newIngredient.getIngredientName(), newIngredient);
        //
        Assert.assertTrue(isItUpdate);
    }

    @Test
    public void updateIngredientInMyListByNameTest2(){
        //
        var ingredient1 = new Ingredient("apple", 1);
        var ingredient2 = new Ingredient("egg", 2);
        Ingredient newIngredient = new Ingredient("apple",2);
        //
        myListService.insertIngredientToMyList(ingredient1);
        myListService.insertIngredientToMyList(ingredient2);

        myList.getMyList().stream().forEach(ing -> System.out.println(ing.toString()));

        var isItUpdate = myListService.
                updateIngredientByName(newIngredient.getIngredientName(), newIngredient);
        System.out.println("-----------after update------------");
        myList.getMyList().stream().forEach(ing -> System.out.println(ing.toString()));
        //
        Assert.assertEquals(2,
                myListService.getMyListByName("apple").get().getIngredientCount());
    }

}
