package com.example.demo.apiTest;

import com.example.demo.api.RecipeController;
import com.example.demo.dao.RecipeListDao;
import com.example.demo.dao.UIdao.UIRecipeListDao;
import com.example.demo.model.Recipe;
import com.example.demo.service.RecipeService;
import com.example.demo.service.UIservice.UIRecipeService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class RecipeListTest {
    private final UIRecipeListDao recipeList = new RecipeListDao();
    private final UIRecipeService recipeService = new RecipeService(recipeList);
    private final RecipeController recipeApi;

    public RecipeListTest() {
        this.recipeApi = new RecipeController(recipeService);
    }

    // post new recipe

    @Test
    public void addNewRecipeApiTest1(){
        //
        Recipe recipe1 = new Recipe("omelet");
        Recipe recipe2 = new Recipe("jomlet");

        //
        recipeApi.addNewRecipe(recipe1);
        recipeApi.addNewRecipe(recipe2);
        //
        Assert.assertFalse(recipeService.getRecipeList().isEmpty());
    }


    @Test
    public void addNewRecipeApiTest2(){
        //
        Recipe recipe1 = new Recipe("omelet");
        Recipe recipe2 = new Recipe("jomlet");

        //
        recipeApi.addNewRecipe(recipe1);
        recipeApi.addNewRecipe(recipe2);
        //
        Assert.assertEquals(2, recipeService.getRecipeList().size());
    }

    // get the recipe list

    @Test
    public void getRecipeListApiTest1(){
        //

        //
        var list = recipeApi.getRecipeList();
        //
        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void getRecipeListApiTest2(){
        //
        Recipe recipe1 = new Recipe("omelet");
        Recipe recipe2 = new Recipe("jomlet");

        //
        recipeService.addNewRecipe(recipe1);
        recipeService.addNewRecipe(recipe2);
        var list = recipeApi.getRecipeList();
        //
        Assert.assertEquals(2, list.size());
    }

    // get recipe by name api test

    @Test
    public void getRecipeByNameFromEmptyRecipeListApi1(){
        //

        //
        var finedRecipe =recipeApi.getRecipeByName("omelet");
        //
        Assert.assertNull(finedRecipe);
    }



    @Test
    public void getRecipeByNameTest1(){
        //
        Recipe recipe1 = new Recipe("omelet");
        Recipe recipe2 = new Recipe("jomlet");

        //
        recipeService.addNewRecipe(recipe1);
        recipeService.addNewRecipe(recipe2);

        var finedRecipe = recipeApi.getRecipeByName("omelet");
        //
        Assert.assertNotNull(finedRecipe);
    }

    @Test
    public void getRecipeByNameTest2() {
        //
        Recipe recipe1 = new Recipe("omelet");
        Recipe recipe2 = new Recipe("jomlet");

        //
        recipeService.addNewRecipe(recipe1);
        recipeService.addNewRecipe(recipe2);

        var finedRecipe = recipeApi.getRecipeByName("omelet");
        //

        Assert.assertTrue(finedRecipe.getRecipeName().equals("omelet"));
    }

    @Test
    public void getRecipeByNameTest3() {
        //
        Recipe recipe1 = new Recipe("omelet");
        Recipe recipe2 = new Recipe("jomlet");

        //
        recipeService.addNewRecipe(recipe1);
        recipeService.addNewRecipe(recipe2);

        var finedRecipe = recipeApi.getRecipeByName("tart");
        //

        Assert.assertNull(finedRecipe);
    }

    //delete recipe by its name api
    @Test
    public void deleteRecipeByNameApiTest(){
        //
        var recipe1 = new Recipe("omelet");
        var recipe2 = new Recipe("jomlet");

        //
        recipeService.addNewRecipe(recipe1);
        recipeService.addNewRecipe(recipe2);

        recipeService.getRecipeList().stream().forEach(recipe -> System.out.println(recipe.toString()));

        recipeApi.deleteRecipeByName("omelet");

        System.out.println("-------------------after delete---------------");
        recipeService.getRecipeList().stream().forEach(recipe -> System.out.println(recipe.toString()));
        //
        Assert.assertEquals(1, recipeService.getRecipeList().size());
    }
}
