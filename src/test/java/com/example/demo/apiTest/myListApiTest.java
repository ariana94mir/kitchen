package com.example.demo.apiTest;

import com.example.demo.api.MyListController;
import com.example.demo.dao.MyListDao;
import com.example.demo.dao.UIdao.UIMyListDao;
import com.example.demo.model.Ingredient;
import com.example.demo.service.MyListService;
import com.example.demo.service.UIservice.UIMyListService;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class myListApiTest {

    private final UIMyListDao myList = new MyListDao();
    private final UIMyListService myListService = new MyListService(myList);
    private final MyListController myListApi;

    public myListApiTest() {
        this.myListApi = new MyListController(myListService);
    }

    // Adder
    @Test
    public void insertIngredientToMyListApiTest(){
        //
        var ingredient1 = new Ingredient("apple", 2);

        //
        myListApi.addIngredientToMyList(ingredient1);

        //
        Assert.assertEquals(1,myList.getMyList().size());
    }

    @Test
    public void insertTwoToMyListApiTest() {
        //
        var ingredient1 = new Ingredient("apple", 2);
        var ingredient2 = new Ingredient("pear", 2);

        //
        myListApi.addIngredientToMyList(ingredient1);
        myListApi.addIngredientToMyList(ingredient2);

        //
        Assert.assertEquals(2, myList.getMyList().size());
    }

    //Getter
    @Test
    public void getEmptyMyListApiTest(){
        //

        //
        var theList = myListApi.getMyIngredientList();

        //
        Assertions.assertEquals(0,theList.size());
    }

    @Test
    public void getMyListOneIngredientApiTest(){
        //
        var ingredient1 = new Ingredient("apple", 1);

        //
        myListService.insertIngredientToMyList(ingredient1);

        var theList = myListApi.getMyIngredientList();

        //
        Assertions.assertEquals(1,theList.size());
    }

    @Test
    public void getMyListTwoIngredientApiTest(){
        //
        var ingredient1 = new Ingredient("apple", 1);
        var ingredient2 = new Ingredient("egg", 2);

        //
        myListService.insertIngredientToMyList(ingredient1);
        myListService.insertIngredientToMyList(ingredient2);

        var theList = myListApi.getMyIngredientList();

        //
        Assertions.assertEquals(2,theList.size());
    }

    //Getter by name

    @Test
    public void getIngredientByNameFromEmptyMyListApiTest2(){
        //

        //
        var ingredient = myListApi.getIngredientByName("apple");

        //
        Assert.assertNull(ingredient);
    }

    @Test
    public  void getIngredientByNameApiTest(){
        //
        var ingredient1 = new Ingredient("apple", 1);
        var ingredient2 = new Ingredient("egg", 2);

        //
        myListService.insertIngredientToMyList(ingredient1);
        myListService.insertIngredientToMyList(ingredient2);
        var ingredient = myListApi.getIngredientByName("apple");

        //
        Assertions.assertEquals(1, ingredient.getIngredientCount());
    }

    @Test
    public  void getIngredientByNameTest2(){
        //
        var ingredient1 = new Ingredient("apple", 1);
        var ingredient2 = new Ingredient("egg", 2);

        //
        myListService.insertIngredientToMyList(ingredient1);
        myListService.insertIngredientToMyList(ingredient2);
        var ingredient = myListApi.getIngredientByName("apple");

        //
        Assertions.assertTrue(ingredient.getIngredientName().equals("apple"));
    }

    // delete by Name





    // update by name



}
